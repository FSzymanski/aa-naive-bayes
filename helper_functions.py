import numpy as np


def load_csv(file_name):
    rows = []
    lines = open(file_name).readlines()
    for line in lines[:]:
        columns = line.split('\t')
        rows.append((float(columns[0]), float(columns[1]), float(columns[2]), float(columns[3]), float(columns[4])))
    return np.array(rows)


def getStandarizedData(data):
    standarized = []
    mean = np.mean(data)
    std = np.std(data)
    for i in data:
        ii = (i - mean) / std
        standarized.append(ii)
    return np.array(standarized)


def splitData(fraction, data):
    _fraction = int((len(data) / fraction))
    trainset = data[:_fraction, :]
    testset = data[_fraction:, :]
    return trainset, testset

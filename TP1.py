import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold
from sklearn import svm
from sklearn.neighbors import KernelDensity
from sklearn.naive_bayes import GaussianNB

from t4_aux import plot_svm
from sklearn.model_selection import GridSearchCV

import helper_functions as help


def separate_by_class(dataset):
    separated = dict()
    separated[0] = list()
    separated[1] = list()
    for i in range(len(dataset)):
        row = dataset[i]
        if row[-1] == 0:
            separated[0].append(row)
        if row[-1] == 1:
            separated[1].append(row)

    separated[0] = np.array(separated[0])
    separated[1] = np.array(separated[1])
    return separated


def get_logs(sep_data, bandwidth=None):
    log_dense = dict()

    if bandwidth is None:
        params = {'bandwidth': np.linspace(0.02, 60, 3000, endpoint=True)}
        grid = GridSearchCV(KernelDensity(), params, cv=5, iid=False)
        grid.fit(sep_data[0])
        bandwidth = grid.best_estimator_.bandwidth
        print("BBBBBBBBBBBB")
        print(bandwidth)

    for i in range(0, 2):               #log_dense[class][feature][data]
        log_dense[i] = dict()
        for j in range(0, 4):
            log_dense[i][j] = list()
            kde = KernelDensity(kernel='gaussian', bandwidth=bandwidth).fit(np.vstack((sep_data[i][:, j], sep_data[i][:, -1])).T)
            log_dense[i][j].append(kde.score_samples(np.vstack((sep_data[i][:, j], sep_data[i][:, -1])).T))
            log_dense[i][j] = np.array(log_dense[i][j])

    return log_dense


def get_min_max(data):
    log_max = dict()
    log_min = dict()

    for i in range(0, 2):               #log_dense[class][feature][data]
        log_max[i] = dict()
        log_min[i] = dict()
        for j in range(0, 4):
            log_max[i][j] = list()
            log_min[i][j] = list()

            log_max[i][j].append(max(data[i][:, j]))
            log_min[i][j].append(min(data[i][:, j]))

            log_max[i][j] = np.array(log_max[i][j])
            log_min[i][j] = np.array(log_min[i][j])

    return log_min, log_max


def classify(base_prob_0,  base_prob_1, data, test_data, log_dens):
    classes_0 = np.zeros(len(test_data[0]))
    classes_1 = np.zeros(len(test_data[0]))

    log_min, log_max = get_min_max(data)

    for row in range(0, len(test_data[0])):             #For class 0
        sum_0 = base_prob_0
        sum_1 = base_prob_1

        for column in range(0, 3):
            #index = (int)((value-min)/(max-min)*length)
            index = int(((test_data[0][row, column]-log_min[0][column])
                         / (log_max[0][column] - log_min[0][column]))*len(log_dens[0][0][0]))
            if index < 0:
                index = 0
            elif index >= len(log_dens[0][0][0]):
                index = len(log_dens[0][0][0]) - 1
            sum_0 = sum_0 + log_dens[0][column][0][index]
            sum_1 = sum_1 + log_dens[1][column][0][index]

        if sum_0 < sum_1:
            classes_0[row] = 1

    for row in range(0, len(test_data[1])):             #For class 1
        index = int(((test_data[1][row, column] - log_min[0][column])
                     / (log_max[0][column] - log_min[0][column])) * len(log_dens[0][0][0]))
        if index < 0:
            index = 0
        elif index >= len(log_dens[0][0][0]):
            index = len(log_dens[0][0][0]) - 1
        sum_0 = sum_0 + log_dens[0][column][0][index]
        sum_1 = sum_1 + log_dens[1][column][0][index]

        if sum_0 < sum_1:
            classes_0[row] = 1

    errors = sum(1 - classes_0) + sum(classes_1)
    errors_perc = float(errors) / (len(classes_0) + len(classes_1))
    return 1 - errors_perc


def plot_bandwidth(bandwidth_ticks, accuracy_tests):
    plt.plot(bandwidth_ticks, accuracy_tests)
    plt.xlabel("Bandwidth")
    plt.ylabel("Accuracy")
    plt.xticks(np.arange(0, 61, step=5))
    plt.savefig('NB1.png', dpi=300)
    plt.close()


def naive(data, testData):
    sep_data = separate_by_class(data)
    sep_test_data = separate_by_class(testData)

    base_prob_0 = np.log(len(sep_data[0])/(len(sep_data[0])+len(sep_data[1])))
    base_prob_1 = np.log(len(sep_data[1])/(len(sep_data[0])+len(sep_data[1])))

    accuracy_tests = []
    best_accuracy = 0
    best_bandwidth = 0
    bandwidth_ticks = np.linspace(0.02, 60, 3000, endpoint=True)

    for i in bandwidth_ticks:        #check best bandwith
        log_dens = get_logs(sep_data, i)
        accuracy_tests.append(classify(base_prob_0,  base_prob_1, sep_data, sep_test_data, log_dens))
        if accuracy_tests[-1] >= best_accuracy:
            best_accuracy = accuracy_tests[-1]
            best_bandwidth = i
        print("i: " + str(i))            #To see whats the progress

    print("Scratch Naive Bayes accuracy: " + str(best_accuracy))
    print("Best bandwidth: " + str(best_bandwidth))

    plot_bandwidth(bandwidth_ticks, accuracy_tests)


def get_gnb_score(train,test):
    clf = GaussianNB()
    clf.fit(train[:, :-1], train[:, -1])
    print("Sklearn GNB score: " + str(clf.score(test[:, :-1], test[:, -1])))


def calc_fold(feats, X, Y, train_ix, valid_ix, C=1):
    """return error for train and validation sets"""
    reg = LogisticRegression(C=C, tol=1e-10)
    reg.fit(X[train_ix, :feats], Y[train_ix])
    prob = reg.predict_proba(X[:, :feats])[:, 1]
    squares = (prob - Y) ** 2
    return np.mean(squares[train_ix]), np.mean(squares[valid_ix])


def folds(data):
    folds = 10
    kf = StratifiedKFold(n_splits=folds)
    for feats in range(1, 5):
        tr_err = va_err = 0
        for tr_ix, va_ix in kf.split(data[:, -1], data[:, -1]):
            r, v = calc_fold(feats, data[:, :4], data[:, -1], tr_ix, va_ix)
            tr_err += r
            va_err += v
        print(feats, ':', tr_err / folds, va_err / folds)


def splitData(fraction, data):
    _fraction = int((len(data) / fraction))
    trainset = data[:_fraction, :]
    testset = data[_fraction:, :]
    return trainset, testset


def svmD(data, dataTest):
    for C in [1]:
        gamma_2d_range = [1e-1, 1, 1e1]
        classifiers = []
        scoress = []
        trainSet, testSet = splitData(2, data)
        for gamma in gamma_2d_range:
            clf = svm.SVC(kernel='rbf', C=C, degree=3, gamma=gamma, coef0=1)
            clf.fit(trainSet[:, :-1], trainSet[:, -1])
            support = clf.support_
            scoress.append(clf.score(dataTest[:, :-1], dataTest[:, -1]))
            classifiers.append((C, gamma, clf, support))

        idx = scoress.index(max(scoress))
        print("SVM Score RBF: " + str(classifiers[idx][2].score(dataTest[:, :-1], dataTest[:, -1])))
        plot_svm(trainSet, classifiers[idx][2], "plot_rbf", C, gamma)


def main():
    data = help.load_csv("TP1_train.tsv")
    data[:, :-1] = help.getStandarizedData(data[:, :-1])
    np.random.shuffle(data)

    dataTest = help.load_csv("TP1_test.tsv")
    dataTest[:, :-1] = help.getStandarizedData(dataTest[:, :-1])
    np.random.shuffle(dataTest)

    folds(data)

    get_gnb_score(data, dataTest)
    naive(data, dataTest)
    svmD(data, dataTest)


if __name__ == "__main__":
    main()
